# Πειραματική Θεμελίωση Φυσικής Στοιχειωδών Σωματιδίων

### Διαλέξεις
Μέσω τηλεδιασκεψης πατώντας [εδώ](https://zoom.us/j/208535514) 


### Λίστα Διαλέξεων
- Μάθημα 1, 20-02-2020: Ανασκόπηση παλαιότερης γνώσης
- Μάθημα 2, 05-03-2020: Ελαστική σκέδαση e-p
- Μάθημα 3, 19-03-2020: Deep Inelastic Scattering
   - To pdf αρχείο με τις διαφάνειες του μαθήματος βρίσκεται εδώ: [Μάθημα3](https://drive.google.com/file/d/1QQVxIglN-frkTqxE0BCHlH1LSEGBd_0p/view?usp=sharing)
- Μάθημα 4, 27-03-2020: QCD - Part 1
   - To pdf αρχείο με τις διαφάνειες του μαθήματος βρίσκεται εδώ: [Μάθημα4](https://drive.google.com/file/d/1_88ysIgKdzpoiayg7EisAbcu7698qd4L/view?usp=sharing)
- Μάθημα 5, 02-04-2020: QCD - Part 2
   - To pdf αρχείο με τις διαφάνειες του μαθήματος βρίσκεται εδώ: [Μάθημα5](https://drive.google.com/file/d/1WjbDNJLfxsdK8p9DcqcADXDYPIQXXBQg/view?usp=sharing)
- Μάθημα 6, 09-04-2020: Lepton weak interactions/Netrino DIS
   - To pdf αρχείο με τις διαφάνειες του μαθήματος βρίσκεται εδώ: [Μάθημα6](https://drive.google.com/file/d/1IjbnC6yMu2GNcqlruxJHX7V-sSvDuN-X/view?usp=sharing)
- Μάθημα 7, 30-04-2020: Ταλαντώσεις νετρίνων
   - To pdf αρχείο με τις διαφάνειες του μαθήματος βρίσκεται εδώ: [Μάθημα7](https://drive.google.com/file/d/1uq93WwGa1uZrxef2UpJ6DWBi6hjL2UEB/view?usp=sharing)
- Μάθημα 8, 14-05-2020: EWK Unification
   - To pdf αρχείο με τις διαφάνειες του μαθήματος βρίσκεται εδώ: [Μάθημα8](https://drive.google.com/file/d/1JrGHbZ0bUma4u7qkhSCWXg0WcShjqOIQ/view?usp=sharing)
- Μάθημα 9, 30-05-2020: SM Tests
   - To pdf αρχείο με τις διαφάνειες του μαθήματος βρίσκεται εδώ: [Μάθημα9](https://drive.google.com/file/d/12w15UIEtW6Hsa4RftmlmcWAiqHU_U2dR/view?usp=sharing)

### Ασκήσεις
- Πρώτο σετ ασκήσεων: 13.7 και 13.8 από το βιβλίο του M. Thomson
- Δεύτερο σετ ασκήσεων: [Ασκήσεις 2](https://drive.google.com/file/d/1JHDQ2zO5HkAYVptW4p6sTM8GetF9nYEz/view?usp=sharing)

### Forum για την ερωτήσεις, απορίες
Γράψτε την ερώτησή σας στο google doc εδώ: [link](https://docs.google.com/document/d/1-Xix1l4MDrmBGJEpYUM6Pd5QXl36Nxuo6kJV6iLUXuA/edit?usp=sharing)